package com.testCompany.testApp;

import java.util.List;

/**
 * Created by Vultur Rares on 3/8/2018.
 */

public class User {

    public String UserID;
    public String DisplayName;
    public int Level;
    public int XP;
    public int Rating;
    public List<String> LinkedDevices;
    public List<Chest> chestsPending;
    public List<Deck> Decks;
    public int SoftCurrency;
    public int HardCurrency;
    public String MatchID;

}
