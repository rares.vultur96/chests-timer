package com.testCompany.testApp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String USERS_NODE = "users";
    private static final String CHESTS_NODE = "chestsPending";

    private User user;
    private List<Chest> pendingChests;
    private List<TextView> chestSlots;
    private Button btnGetChest;
    private TextView tvChest1;
    private TextView tvChest2;
    private TextView tvChest3;
    private TextView tvChest4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

        login();

    }

    private void initViews() {
        btnGetChest = findViewById(R.id.getChestButton);
        pendingChests = new ArrayList<>();
        chestSlots = new ArrayList<>();
        tvChest1 = findViewById(R.id.chest_1);
        chestSlots.add(tvChest1);
        tvChest2 = findViewById(R.id.chest_2);
        chestSlots.add(tvChest2);
        tvChest3 = findViewById(R.id.chest_3);
        chestSlots.add(tvChest3);
        tvChest4 = findViewById(R.id.chest_4);
        chestSlots.add(tvChest4);
    }

    private void login() {
        FirebaseAuth.getInstance().signInWithEmailAndPassword("test@test.com", "qqqqqqqqqq")
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            readUser(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        }
                    }
                });
    }

    private void readUser(final String uid) {
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference("users");
        reference.child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
                getChestInfo();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getChestInfo() {
        if (user.chestsPending.size() < 4) {
            btnGetChest.setOnClickListener(new GetChestOnButtonClick());
        }
        pendingChests = user.chestsPending;
        for (int i = 0; i < 4; i++) {
            if (i + 1 <= pendingChests.size()) {
                chestSlots.get(i).setText(pendingChests.get(i).arena + ", " +
                        pendingChests.get(i).type + ", " +
                        pendingChests.get(i).lockedUntilTimestamp);

                chestSlots.get(i).setOnClickListener(new PerformChestUnlockActionOnClick(pendingChests.get(i), chestSlots.get(i)));
            }
        }

        Log.d("Chests", String.valueOf(pendingChests.size()));
    }

    private void saveChestsInDatabase(List<Chest> chests) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(USERS_NODE)
                .child(user.UserID)
                .child(CHESTS_NODE);
        reference.setValue(chests);
    }

    class GetChestOnButtonClick implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Chest chest = new Chest();
            chest.type = "silver";
            chest.lockedUntilTimestamp = getChestTimeStamp(3);
            chest.arena = 6;
            user.chestsPending.add(chest);


            saveChestsInDatabase(user.chestsPending);
            getChestInfo();
        }

        private String getChestTimeStamp(int hours) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.HOUR, hours);
            Timestamp timestamp = new Timestamp(calendar.getTimeInMillis());
            return String.valueOf(timestamp);
        }
    }

    class PerformChestUnlockActionOnClick implements View.OnClickListener {

        private Chest chest;
        private TextView textView;

        public PerformChestUnlockActionOnClick(Chest chest, TextView textView) {
            this.chest = chest;
            this.textView = textView;
        }

        @Override
        public void onClick(View view) {
            if (chest.timestampWhenUnlockStarted == null) {
                chest.timestampWhenUnlockStarted = String.valueOf(ServerValue.TIMESTAMP);
                textView.setText(chest.arena + ", " +
                        chest.type + ", " +
                        chest.lockedUntilTimestamp + ", " +
                        chest.timestampWhenUnlockStarted);
                saveChestsInDatabase(pendingChests);
            } else {
                chest.lockedUntilTimestamp = String.valueOf(ServerValue.TIMESTAMP);
                Toast.makeText(MainActivity.this, "You unlocked and opened this chest", Toast.LENGTH_SHORT).show();
                pendingChests.remove(chest);
                textView.setText("");
                user.chestsPending = pendingChests;
                saveChestsInDatabase(pendingChests);
            }
        }
    }
}
